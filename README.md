# Clopter
### Because command line option parsing doesn't have to be complicated

## Desired Functionality
1. `["key"]` access notation
2. Easy access to `value`
3. Easy access to `is_present`
4. Default behavior for quick use case
5. Simple setup for modified use case
6. Inuitive API for complex behavior use case
7. No global variables to avoid namespace collisions
8. Automatically build help form with API for modified use or use your own

## Example Usage
```c++
#include "clopter.h"

int main(int argc, char** argv)
{
    clopter cyclopts = new clopter();

    cyclopts.parse(argc, argv);

    if (cyclopts["blarg"]) {
        // Run commands with "blarg" value
    }

    if (cyclopts["verbose"]) {
        // Run verbose statements with verbose level
    }

    if (cyclopts["help"]) {
        // Implement your own help formatting
    }
}
```