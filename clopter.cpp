#include "clopter.h"
#include "IClopter.h"
#include "Cloption.h"
#include "Hangar.h"
#include <iostream>
#include <string>
#include <queue>
#include <stack>
#include <exception>

using namespace std;

ICloption clopter::self_count(ICloption* inputOption)
{
    throw exception("Not implemented");
    return ICloption();
}

ICloption clopter::val_count(ICloption* inputOption)
{
    throw exception("Not implemented");
    return ICloption();
}

ICloption clopter::val_read(ICloption* inputOption)
{
    throw exception("Not implemented");
    return ICloption();
}

void clopter::Evaluate(queue<Token> inputQueue, stack<ICloption> outputStack)
{
    if (_blueprint.empty()) {
        return;
    }

    for (size_t i = 0; i < inputQueue.size(); i++) {
        Token iTok = inputQueue.front();
        inputQueue.pop();

        // Check if input option label matches any stored in the hangar
        if (_blueprint.contains(iTok.Label)) {
            Cloptken cycken(_blueprint[iTok.Label]);
            switch (cycken.ValueLocation) {
            case VAL_LOC_SELF:
                cycken.Value = iTok.Label;
                break;
            case VAL_LOC_SELF_COUNT:
                cycken.Value = self_count(iTok);
                break;
            case VAL_LOC_SELF_BOOL:
                cycken.Value = val_count(iTok);
                break;
            case VAL_LOC_POST:
                cycken.Value = val_read(inputQueue.front());
                inputQueue.pop();
                break;
            case VAL_LOC_PRE:
                cycken.Value = val_read(outputStack.top());
                outputStack.pop();
                break;
            case VAL_LOC_UNKNOWN:
            default:
                throw invalid_argument("Error linking a cloptken to value.");
            }

            cloptionStack.push(cycken);
        }
    }
}

Hangar clopter::Make(stack<ICloption> inputStack)
{
    Hangar hangar;
    for (size_t i = 0; i < inputStack.size(); i++) {
        ICloption iClopt = inputStack.top();
        inputStack.pop();
        hangar.add(iClopt);
    }
    return hangar;
}

////////////
// Public //
////////////

clopter::clopter()
{
}

void clopter::Option_Short(char shrt = '\0', char* help = '\0')
{
    throw exception("Not yet implemented");
}

void clopter::Option_Long(char* lng = '\0', char* help = '\0')
{
    throw exception("Not yet implemented");
}

void clopter::Option(char* shrt = '\0', char* lng = '\0', char* help = '\0')
{
    Cloption cycloption;
    if (*shrt != '\0') {
        cycloption.shortf = *++shrt;
    }
    if (*lng != '\0') {
        cycloption.longf = *(lng + 2);
    }
    if (*help != '\0') {
        cycloption.Description = help;
    }

    cycloption.Type = OPT_TYPE_LONG;
    cycloption.ValueType = VAL_TYPE_REQUIRED;
    cycloption.CXXType = "string";
}

Hangar clopter::parse(int argc, char** argv)
{
    queue<Token> tokenQueue;
    for (size_t i = 0; i < argc; i++) {
        Token iTok(argv[i]);
        tokenQueue.push(iTok);
    }

    // Connect options to values
    stack<ICloption> cloptionStack;
    Evaluate(tokenQueue, cloptionStack);

    return Make(cloptionStack);
}
