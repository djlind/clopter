#pragma once
#include "IClopter.h"

struct ICloption {
    OptType Type;
    char *Label;
    ICloption *_next = nullptr;
    ICloption():
        Type(OPT_TYPE_UNKNOWN),
        Label("")
    {}
    ICloption(OptType type, char *label):
        Type(type),
        Label(label)
    {}
};

struct Token: public ICloption {
    Token(): ICloption()
    {}
    Token(OptType type, char *label):
        ICloption(type, label)
    {}
    Token(char *arg)
    {
        if (*arg == '-') {
            if (*(++arg) == '-') {
                Type = OPT_TYPE_LONG;
                Label = arg++;
            }
            else {
                Type = OPT_TYPE_SHORT;
                Label = arg;
            }
        }
        else {
            Type = OPT_TYPE_VALUE;
            Label = arg;
        }
    }
};

struct Cloptken: public ICloption {
    static auto Value;
    ValType ValueType;
    ValLoc ValueLocation;

    Cloptken():
        ICloption(),
        ValueType(VAL_TYPE_REQUIRED),
        ValueLocation(VAL_LOC_UNKNOWN)
    {}
    Cloptken(char *label, OptType otype = OPT_TYPE_UNKNOWN, ValType vtype = VAL_TYPE_UNKNOWN, ValLoc valloc = VAL_LOC_UNKNOWN):
        ICloption(otype, label),
        ValueType(vtype),
        ValueLocation(valloc)
    {}
    Cloptken(Cloption& iClopt):
        ICLoption(iClopt.Type, iClopt.Label),
        ValueType(iClopt.ValueType),
        ValueLocation(iClopt.ValueLocation)
    {}
};

struct Cloption: public ICloption {
    char *Description;
    static auto Value;
    ValType ValueType;
    ValLoc ValueLocation;
    char *CXXType;
    char shortf;
    char *longf;

    Cloption():
        ICloption()
    {}
    Cloption(char *label, OptType otype, ValType vtype, ValLoc vloc):
        ICloption(otype, label),
        ValueType(vtype),
        ValueLocation(vloc)
    {}
};
