#pragma once
#include "Cloption.h"

class Hangar {
private:
    ICloption* _start = nullptr;
    int _count;
    ICloption* find(char *label);
public:
    Hangar():
        _count(0)
    { 
        Cloption("default", OPT_TYPE_FLAG, VAL_TYPE_BOOL, VAL_LOC_SELF) def;
        push(def);
    }
    ~Hangar() { clear(); }
    void push(ICloption *nClopt);
    void remove(char *label);
    bool contains(char *label);
    void clear();
    int size();
    bool empty();
    void printHelp();
    auto operator[](char *pos);
};
