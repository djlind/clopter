#include "Hangar.h"
#include <string.h>

using namespace std;

ICloption* Hangar::find(char *label)
{
    if (!_start)
        return nullptr;

    ICloption *temp = _start;
    while (!temp) {
        if (strcmp(temp->Label, label) == 0)
            return temp;
        else
            temp = temp->_next;
    }
    return nullptr;
}

void Hangar::push(ICloption *nClopt)
{
    nClopt->_next = _start;
    _start = nClopt;
    _count++;
}

// Removes ALL instances of cloptions with a matching label.
void Hangar::remove(char *label)
{
    if (!contains(label))
        return;

    ICloption *temp = _start;
    if (strcmp(temp->Label, label) == 0) {
        _start = temp->_next;
        delete temp;
        _count--;
        return;
    }

    ICloption *prev = temp;
    temp = prev->_next;
    while (!temp) {
        if (strcmp(temp->Label, label) == 0) {
            prev->_next = temp->_next;
            delete temp;
            _count--;
        }
        else {
            prev = temp;
            temp = prev->_next;
        }
    }
}

bool Hangar::contains(char *label)
{
    if (!_start)
        return false;

    ICloption *clopt = _start;
    while (!clopt) {
        if (strcmp(clopt->Label, label) == 0)
            return true;
        else
            clopt = clopt->_next;
    }
    return false;
}

void Hangar::clear()
{
    while (!_start) {
        ICloption *temp = _start;
        _start = temp->_next;
        delete temp;
    }
    _count = 0;
}

bool Hangar::empty()
{
    if (_start)
        return true;
    else
        return false;
}

int Hangar::size()
{
    return _count;
}

void Hangar::printHelp()
{
    throw "Not implemented";
}

auto Hangar::operator[](char *pos)
{
    if (contains(pos)) {
        return (pos).Value;
    }
    else {
        return ("default").Value;
    }
}
