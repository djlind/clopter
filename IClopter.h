#pragma once
#include <string>
#include <queue>

enum OptType {
	OPT_TYPE_UNKNOWN,	//First enum always unknown
    OPT_TYPE_FLAG,      //Stackable option type with no value '[-[-]]option'
	OPT_TYPE_SHORT,     //Discrete option type with a value '[value] -o [value]'
	OPT_TYPE_LONG,      //Discrete option type with a value '[value] --option [value]'
    OPT_TYPE_COMMAND,   //Discrete option type with a value 'option [value]'
	OPT_TYPE_VALUE,     //Discrete value type whose text is a value 'value'
    OPT_TYPE_ANON       //Generic option type whose text is a value 'value'
};

enum ValLoc {
    VAL_LOC_UNKNOWN,    //First enum always unknown
    VAL_LOC_PRE,        //Value which appears before an option '<value> [-[-]]option'
    VAL_LOC_POST,       //Value which appears after an option '[-[-]]option <value>'
    VAL_LOC_SELF,       //Descrete option type 'option' whose text is the value
    VAL_LOC_SELF_BOOL,  //Stackable option type '[-[-]]option' which inverts a boolean
    VAL_LOC_SELF_COUNT  //Stackable option type '[-[-]]option' which increments a count
};

enum ValType {
	VAL_TYPE_UNKNOWN,		//First enum always unknown
	VAL_TYPE_COUNT,         //Stackable option type '-xvpf' which increments count
	VAL_TYPE_BOOL,          //Stackable option type '-ddd' which switches bool
	VAL_TYPE_REQUIRED,		//Generic discrete option type, default is error
	VAL_TYPE_OPTIONAL,		//Generic discrete option type, no default
	VAL_TYPE_VALUE			//Value type 'ABCD' to be used with an option
};

class IClopter
{
public:
	virtual void Option(std::string shrt, std::string lng, std::string help) = 0;
};


class ClopterFactory
{
public:
	static IClopter* GetClopter();
};
