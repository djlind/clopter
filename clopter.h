#pragma once
#include "IClopter.h"
#include "Cloption.h"
#include "Hangar.h"
#include <queue>
#include <stack>

class clopter :
	public IClopter
{
private:
	Hangar _blueprint;
	int _count;
	bool _hasOptions;
	void Evaluate(std::queue<Token> inputQueue, std::stack<ICloption> outputStack);
	Hangar Make(std::stack<ICloption> inputStack);
public:
	clopter();
	//void Command(char* cmd, char* help);
	void Option_Short(char shrt, char* help);
	void Option_Long(char* lng, char* help);
	void Option(char* shrt, char* lng, char* help);
	Hangar parse(int argc, char** argv);
};

